<?php

declare(strict_types=1);

namespace App\MessageHandler\Consumer;

use App\Service\MailClient;
use App\Repository\ConsumerRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ConsumerMailMessageHandler implements MessageHandlerInterface
{
    private MailClient $client;
    private string $alertMessage;
    private ConsumerRepository $repository;

    public function __construct(MailClient $client, ConsumerRepository $repository)
    {
        $this->client = $client;
        $this->repository = $repository;
        $this->alertMessage = 'Hello world';
    }

    public function __invoke(ConsumerMailMessage $message): void
    {
        $consumer = $this->repository->find($message->getConsumerId());

        if (null === $consumer) {
            throw new \LogicException("There's no consumer with id: {$message->getConsumerId()}");
        }

        $this->client->sendMessage($consumer->getEmail(), $this->alertMessage);
    }
}