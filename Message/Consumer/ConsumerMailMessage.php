<?php

declare(strict_types=1);

namespace App\Message\Consumer;

final class ConsumerMailMessage
{
    private int $consumerId;

    public function __construct(int $consumerId)
    {
        $this->consumerId = $consumerId;
    }

    public function getConsumerId(): int
    {
        return $this->consumerId;
    }
}