<?php

declare(strict_types=1);

namespace Tests\Controller\Consumer;

use App\Controller\ConsumerController;
use Tests\TransactionalBaseTestCase;

final class ConsumerControllerTest extends TransactionalBaseTestCase
{
    public function providerGetListAction(): array
    {
        return [
            [
                'name' => 'test',
                'email' => 'test@test.ru'
            ],
            [
                'name' => 'test2',
                'email' => 'test2@test.ru'
            ],
            [
                'name' => 'test3',
                'email' => 'test3@test.ru'
            ],
        ];
    }

    /**
     * @dataProvider providerGetListAction
     *
     * @see ConsumerController::getListAction()
     *
     * @param array $expectedConsumers
     *
     * @throws \JsonException
     */
    public function testGetListAction(array $expectedConsumers): void
    {
        $this->createAuthenticatedClient(self::MANAGER_CLIENT_ID);
        $this->get('/api/v1/consumer');

        $content = $this->client->getResponse()->getContent();
        $this->assertHttpCodeIs(Response::HTTP_OK, $content);

        $data = \json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        self::assertSame($expectedConsumers, $data);
    }
}
