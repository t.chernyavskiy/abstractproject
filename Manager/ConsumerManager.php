<?php

declare(strict_types=1);

namespace App\Manager;

use App\Mapper\DtoMapper;
use App\Repository\ConsumerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class ConsumerManager
{
    private EntityManagerInterface $em;
    private ConsumerRepository $consumerRepository;
    private DtoMapper $dtoMapper;
    private MessageBus $messageBus;

    public function __construct(
        EntityManagerInterface $em,
        DtoMapper $dtoMapper,
        MessageBus $messageBus,
        ConsumerRepository $consumerRepository
    )
    {
        $this->em = $em;
        $this->consumerRepository = $consumerRepository;
        $this->dtoMapper = $dtoMapper;
        $this->messageBus = $messageBus;
    }

    /**
     * @param CreateConsumerDto $dto
     *
     * @return int
     */
    public function create(CreateConsumerDto $dto): int
    {
        $consumer = new Consumer();
        $this->dtoMapper->mapToEntity($dto, $consumer);
        $this->em->persist($consumer);
        $this->em->flush();
        $this->messageBus->dispatch(new ConsumerMailMessage($consumer->getId()));

        return $consumer->getId();
    }

    public function getAll()
    {
        $this->consumerRepository->findAll();
    }
}
