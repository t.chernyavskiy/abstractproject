<?php

declare(strict_types=1);

namespace App\Controller;

use App\Manager\ConsumerManager;
use App\Mapper\RequestMapper;

final class ConsumerController extends AbstractController
{
    private ConsumerManager $manager;
    private RequestMapper $requestMapper;

    public function __construct(ConsumerManager $manager, RequestMapper $requestMapper) {
        $this->manager = $manager;
        $this->requestMapper = $requestMapper;
    }

    /**
     * @Route("/api/v1/consumer", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function getListAction()
    {
        $consumer = $this->manager->getAll();

        return $this->json($consumer, Response::HTTP_OK);
    }

    /**
     * @Route("/api/v1/consumer", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postAction(Request $request): JsonResponse
    {
        /** @var CreateConsumerDto $dto */
        $dto = $this->requestMapper->mapToDto($request->getContent(), CreateConsumerDto::class, JsonEncoder::FORMAT);
        $id = $this->manager->create($dto);

        return $this->json(['id' => $id], Response::HTTP_CREATED);
    }
}
